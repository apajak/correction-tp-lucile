# TP2

## IsPrime
> Un nombre premier est un entier naturel qui admet exactement deux diviseurs distincts entiers et positifs. Ces deux diviseurs sont 1 et le nombre considéré, puisque tout nombre a pour diviseurs 1 et lui-même, les nombres premiers étant ceux qui ne possèdent pas d'autre diviseur.

### Objectif:
Faire un algorithme testant si le nombre donné en argument est un nombre premier. <br>

### Methode:

> Les nombres premiers sont forcément impaire so on peut gagner du temps de calcule! 😉

La logique que j'ai trouvé c'est que pour l'argument je teste tous les nombres en partant de 3 jusqu'à la racine carrée de l'argument +1 (pour que ce soit seulement les impaire). Le troisième paramètre dans ma boucle c'est l'incrément. Par défaut il est de 1 mais dans une boucle range on peut lui attribuer une valeur. Du coup j'avance de deux en deux afin de rester sur les impaires !
```py
for i in range(3, int(number**0.5) + 1, 2):
```
Bon on a la boucle maintenant il ne reste plus qu'à vérifier si l'argument peut être divisé par ce nombre générer dans la boucle. Si le résultat du modulo est 0 cela veut dire que ce nombre est un diviseur de l'argument donc c'est tpo bon. Une fois arriver à la fin de la boucle si je n'ai ien trouvé c'est qu'il n'est pas premier.



## code:
[suggestion de code](isprime.py)
