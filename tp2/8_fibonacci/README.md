# TP2

## Fibonacci


> En mathématiques, la suite de Fibonacci est une suite de nombres entiers dont chaque terme successif représente la somme des deux termes précédents, et qui commence par 0 puis 1. Ainsi, les dix premiers termes qui la composent sont 0, 1, 1, 2, 3, 5, 8, 13, 21 et 34.  Cette suite à la logique simple est considérée comme le tout premier modèle mathématique en dynamique des populations.

> Mais si cette suite est aussi célèbre aujourd’hui, c’est parce qu’elle a un taux de croissance exponentiel qui tend vers le nombre d’or, un ratio symbolisé par « φ », associé à de nombreuses qualités esthétiques au sein de notre civilisation. Sa valeur exacte est de (1+√5)/2, ayant comme dix premières décimales 1,6180339887… Ce rapport, considéré comme la clé de l’harmonie universelle, se décline et se transpose par des formes géométriques telles que le rectangle, le pentagone et le triangle.

### Objectif:
Faire un petit algorithme donnant pour un index de la suite de Fibonacci la somme des deux index précédents. <br>

### Methode:
J'ai faits ça d'une manière assez simple avec la récursivité. En définissant les deux premiers résultats comme mes conditions d'arrêt de récursivités puis autrement le résultat est l'appel de la fonction à l'indice -1 + la fonction a l'indice -2.

## code:
[suggestion de code](fibonacci.py)
