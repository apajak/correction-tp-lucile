# TP-2 recursivePower
# Alexandre Pajak

# Instructions:

# Write a recursive function that returns the value at the position index in the fibonacci sequence.
# The first value is at index 0.
# The sequence starts this way: 0, 1, 1, 2, 3 etc...
# A negative index will return -1.
# for is forbidden for this exercise.


# Expected function:
# def fibonacci(index int) -> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(fibonacci(4))


# And its output :
# % python3 fibonacci.py
# 3
# %

### code ###


def fibonacci(index: int) -> int:
    if index >= 0:
        if index == 0:
            return 0
        if index == 1:
            return 1
        if index > 1:
            return fibonacci(index - 1) + fibonacci(index - 2)
    else:
        return -1


def main():
    print(fibonacci(4))


main()
