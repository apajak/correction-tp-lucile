# TP2

## FindNextPrime
> Un nombre premier est un entier naturel qui admet exactement deux diviseurs distincts entiers et positifs. Ces deux diviseurs sont 1 et le nombre considéré, puisque tout nombre a pour diviseurs 1 et lui-même, les nombres premiers étant ceux qui ne possèdent pas d'autre diviseur.

### Objectif:
Cette fois si le but est de trouver le prochain nombre premier en partant de l'argument de la fonction. <br>

### Methode:

On a déjà la fonction qui teste si un nombre est premier ou non. Alors on ne s'embête pas et on la réutilise.
On peut directement importer un fichier que l'on a créé lui-même de la même manière qu'un package extérieur comme pourrait l'être `numpy`. <br>
Si notre fichier.py est au même niveau d'arborescence il suffit de faire :
```py
import isprime
```
Ensuite on l'appelle comme tout les autres package c'est-à-dire comme un objet python. <br>
On fait donc lenomdupackage.lafonctionquejeveux(). <br>

Une fois que j'ai de quoi tester si un nombre est premier je n'ai qu'à lancer une boucle qui part de l'argument et qui teste tous les nombres suivants jusqu'à en trouver un. 



## code:
[suggestion de code](isprime.py)
