# TP-2 isprime
# Alexandre Pajak

# Instructions:

# Write a function that returns true if the int passed as parameter is a prime number. Otherwise it returns false.
# The function must be optimized in order to avoid time-outs with the tester.
# (We consider that only positive numbers can be prime numbers)
# (We also consider that 1 is not a prime number)


# Expected function:
# def isprime(number: int) -> bool:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(isprime(5))
#   print(isprime(4))


# And its output :
# % python3 isprime.py
# True
# False
# %

### code ###


def isprime(number: int) -> bool:
    if number == 2 or number == 3:
        return True
    if number % 2 == 0 or number < 2:
        return False
    for i in range(3, int(number**0.5) + 1, 2):  # only odd numbers
        if number % i == 0:
            return False

    return True


def main():
    print(isprime(5))
    print(isprime(4))


main()
