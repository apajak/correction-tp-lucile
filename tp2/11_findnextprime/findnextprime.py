# TP-2 findnextprime

# Alexandre Pajak

# Instructions:

# Write a function that returns the first prime number that is equal or superior to the int passed as parameter.
# The function must be optimized in order to avoid time-outs with the tester.
# (We consider that only positive numbers can be prime numbers)


# Expected function:
# def findNextPrime(number: int) -> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(findNextPrime(5))
#   print(findNextPrime(4))


# And its output :
# % python3 findnextprime.py
# 5
# 5
# %

### code ###
import isprime


def findNextPrime(number: int) -> int:
    prime = False
    while not prime:
        if isprime.isprime(number):
            prime = True
            return number
        else:
            number += 1


def main():
    print(findNextPrime(5))
    print(findNextPrime(4))


main()
