# TP2

## RecursiveFactorial

### Objectif:
Okai ! Meme chose que avant mais avec de la récursivitée au lieux des boucles. <br>

### Methode:
Du coup on commence par notre condition d'arret de récursivité : <br>
```py
if nb == 1:
    return nb
```
Si nb est =! 1 alors on fait appel a notre fonction avec nb - 1 : <br>
```py 
return nb * recursiveFactorial(nb - 1)
```

## code:
[suggestion de code](recursivefactorial.py)
