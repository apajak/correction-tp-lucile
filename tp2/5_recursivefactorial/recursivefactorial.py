# TP-2 recursiveFactorial
# Alexandre Pajak

# Instructions:

# Write a recursive function that returns the factorial of the int passed as parameter.
# Errors (non possible values or overflows) will return 0.
# for is forbidden for this exercise.


# Expected function:
# def recursiveFactorial(nb: int)-> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(recursiveFactorial(4))


# And its output :
# % python3 recursivefactorial.py
# 24
# %

### code ###


def recursiveFactorial(nb: int) -> int:
    if 1 <= nb <= 20:
        if nb == 1:
            return nb
        else:
            return nb * recursiveFactorial(nb - 1)
    else:
        return 0


def main():
    print(recursiveFactorial(20))


main()
