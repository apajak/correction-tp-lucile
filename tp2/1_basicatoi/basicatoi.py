# TP-2 basicatoi
# Alexandre Pajak

# Instructions:

# Write a function that simulates the behaviour of the Atoi function in Go.
# Atoi transforms a number defined as a string in a number defined as an int.

# Atoi returns 0 if the string is not considered as a valid number.
# For this exercise only valid string will be tested. They will only contain one or several digits as characters.

# For this exercise the handling of the signs + or - does not have to be taken into account.
# This function will only have to return the int. For this exercise the error return of Atoi is not required.

# Expected function:
# def basicAtoi(s str)-> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(basicAtoi("12345"))
#   print(basicAtoi("0000000012345"))
#   print(basicAtoi("000000"))


# And its output :
# % python3 basicatoi.py
# 12345
# 12345
# 0
# %

### code ###


def isValidInput(strNumbers: str) -> bool:
    isValid = bool
    if isinstance(strNumbers, str):
        for num in strNumbers:
            if 47 < ord(num) < 58:
                isValid = True
            else:
                isValid = False
                break
    else:
        isValid = False
    return isValid


# Convert str to ascii and remove 48 (ascii of "0") and multyply the result by n*10 (for unit)
def basicAtoi(strNumbers: str) -> int:
    result = 0
    coefficient = 1
    index = len(strNumbers) - 1
    if isValidInput(strNumbers):
        while index >= 0:
            result += (ord(strNumbers[index]) - 48) * coefficient
            index -= 1
            coefficient *= 10
        return result
    else:
        return 0


def main():
    print(basicAtoi("000123"))


main()
