# TP2

## BasicAtoi

### Objectif:
L'objectif est de créer une version simplifier de la trés populaire fonction `Atoi`. 
Atoi c'est l'acronyme anglais pour "ASCII to integer". <br>
Comme son nom l'indique le but est de convertir un chiffre en ASCII a un chiffre Integer..<br>
Cette fonction est native dans Python. Mais c'est toujours cool de savoir comment ça fonctionne 😉 <br>
Alors c'est parti pour convertir une string en Int !


### Methode:
Première étape comme toujours ln vérifie les inputs. Si la string comporte bien que des chiffres alors on passe au traitement de la string. <br> 
Ma solution pour résoudre ce petit problème c'est de comparer chaque caractère de la string en partant de la fin. <br>
Pour chaque caractère de la string: <br>
- Je récupère sa valeur ASCII avec `ord()`
- Je soustrais 48 a sa valeur ASCII (48 c'est la valeur de 0 en ASCII)<br>
> les codes ASCII des chiffres vont de 48 a 57 alors en soustrayant 48 je récupère la valeur du chiffre 😌
- J'ai donc un Int que je multiplie par un coefficient pour avoir la bonne unité ("124" lorsque je récupère 2 je le multiplie par 10 pour pour q'il devienne 20)
- J'ajoute mon résultat a une variable.
- A chaque tour de boucle je multiplie par 10 mon coéfficient.
- Il ne reste qu'a `print()`!   


## code:
[suggestion de code](basicatoi.py)
