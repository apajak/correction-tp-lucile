# TP-2 iterativePower
# Alexandre Pajak

# Instructions:

# Write an iterative function that returns the value of nb to the power of power.
# Negative powers will return 0. Overflows do not have to be dealt with.


# Expected function:
# def iterativePower(nb: int, power: int)-> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(iterativePower(4, 3))


# And its output :
# % python3 iterativepower.py
# 64
# %

### code ###


def iterativePower(nb: int, power: int) -> int:
    result = nb
    if 0 < power:
        for _ in range(0, power - 1):
            result *= nb
        return result
    else:
        return 1


def main():
    print(iterativePower(4, 3))


main()
