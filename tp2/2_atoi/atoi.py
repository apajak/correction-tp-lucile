# TP-2 atoi
# Alexandre Pajak

# Instructions:

# Write a function that simulates the behaviour of the Atoi function in Go.
# Atoi transforms a number represented as a string in a number represented as an int.

# Atoi returns 0 if the string is not considered as a valid number.
# For this exercise non-valid string chains will be tested. Some will contain non-digits characters.

# For this exercise the handling of the signs + or - does have to be taken into account.
# This function will only have to return the int. For this exercise the error result of Atoi is not required.

# Expected function:
# def atoi(s str)-> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(atoi("12345"))
#   print(atoi("0000000012345"))
#   print(atoi("012 345"))
#   print(atoi("Hello World!"))
#   print(atoi("+1234"))
#   print(atoi("-1234"))
#   print(atoi("++1234"))
#   print(atoi("--1234"))


# And its output :
# % python3 basicatoi.py
# 12345
# 12345
# 0
# 0
# 1234
# -1234
# 0
# 0
# %

### code ###


def atoi(strNumbers: str) -> int:
    result = 0
    coefficient = 1
    index = len(strNumbers) - 1
    while index >= 0:
        if (ord(strNumbers[index]) - 48) < 0 or (ord(strNumbers[index]) - 48) > 9:
            return 0
        else:
            result += (ord(strNumbers[index]) - 48) * coefficient
            index -= 1
            coefficient *= 10
        if ord(strNumbers[index]) == 43 and index == 0:
            return result
        if ord(strNumbers[index]) == 45 and index == 0:
            return -result
    return result


def main():
    print(atoi("12345"))
    print(atoi("0000000012345"))
    print(atoi("012 345"))
    print(atoi("Hello World!"))
    print(atoi("+1234"))
    print(atoi("-1234"))
    print(atoi("++1234"))
    print(atoi("--1234"))


main()
