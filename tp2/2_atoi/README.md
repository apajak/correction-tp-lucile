# TP2

## Atoi

### Objectif:
Bon pour celui-là c'est la même chose que le précédent atoi mais avec la gestion des signes...


### Methode:
Ici j'ai décidé de directement faire les vérifications dans la fonction atoi. <Br> 
Le reste de la logique est la même. Une boucle qui commence de la fin, pour chaque caractère on vérifie s'il est compris entre les bonnes valeurs de la table ASCII. Et pour le dernier caractère on regarde si c'est un plus (43) ou un moins (45) et on retourne le résultat en fonction. 
> Attention la détection du signe ne doit pas se faire seulement sur le dernier caractère. autrement on se retrouverait avec des signes en plein milieu de du string et ça retournerait n’importe quoi !


## code:
[suggestion de code](atoi.py)
