# TP2

## IterativeFactorial

### Objectif:
Le but est de calculer de manière itérative (en gros une fonction procédurale. Avec des boucles for par exemple) la factorielle d'un nombre. <br>



### Methode:
En mathématiques, la factorielle d'un entier naturel n est le produit des nombres entiers strictement positifs inférieurs ou égaux à n. <br>
La factorielle de 4 sera donc `4! = 1 × 2 × 3 × 4 = 24` <br>

À partir de là, tu sais faire donc on va abréger !

## code:
[suggestion de code](iterativefactoial.py)
