# TP-2 IterativeFactorial
# Alexandre Pajak

# Instructions:

# Write an iterative function that returns the factorial of the int passed as parameter.
# Errors (non possible values or overflows) will return 0.


# Expected function:
# def iterativeFactorial(nb: int)-> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(iterativeFactorial(4))


# And its output :
# % python3 iterativefactorial.py
# 24
# %

### code ###


def iterativeFactorial(nb: int) -> int:
    result = 1
    if 0 <= nb <= 20:
        for index in range(1, nb + 1):
            result *= index
        return result
    else:
        return 0


def main():
    print(iterativeFactorial(4))


main()
