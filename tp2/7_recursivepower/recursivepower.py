# TP-2 recursivePower
# Alexandre Pajak

# Instructions:

# Write a recursive function that returns the value of nb to the power of power.
# Negative powers will return 0. Overflows do not have to be dealt with.
# for is forbidden for this exercise.


# Expected function:
# def recursivePower(nb: int, power: int)-> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(recursivePower(4, 3))


# And its output :
# % python3 recursivepower.py
# 64
# %

### code ###


def recursivePower(nb: int, power: int) -> int:
    if power == 0:
        return 1
    if power > 0:
        return nb * recursivePower(nb, power - 1)
    return 0


def main():
    print(recursivePower(4, 3))


main()
