# TP2

## RecursivePopwer

### Objectif:
The same mais en récursif. <br>

### Methode:
La récursivité c'est pas intuitif dutout mais c'est bien pratique alors je te conseil d'en faire un maximum.

Alors comme toujours en récursif : <br>

- Notre condition d'arret de récursivitée : <br>
```py
if power == 0:
    return 1
```
- Sinon on fait appel a sois-même a l'index - 1 :
```py
return nb * recursivePower(nb, power - 1)
```
> La fonction est déja présente dans Python avec `pow(a,b)` où on met `a` a la puissance `b`.

## code:
[suggestion de code](recursivepower.py)
