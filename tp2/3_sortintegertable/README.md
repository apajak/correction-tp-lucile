# TP2

## SortIntegerTable

### Objectif:
Le but est de trier une liste d'Int. c'était Obvious mais bon je suis le shéma. 🤗 <br>



### Methode:
#### Première méthode:
Ok ! Tu as sans doute vu que la seconde méthode était bien mieux mais j'y ai vu ici l'occasion de te montrer une méthode plutôt utile. ☝️ <br>
Le **swap** !
Alors le swap ça consiste a créé des variables temporaires dans le but d'échanger la valeur de deux variables. C'est vraiment très malin tu vas voir.

```py
## SWAP ##
valeur1 = 2
valeur2 = 3
temp1 = int
temp2 = int

temp1 = valeur1
temp2 = valeur2
valeur1 = temp2
valeur2 = temp1

print(f"valeur 1 = {valeur1}")
print(f"valeur 2 = {valeur2}")
```
```py
(base) paja@paja-mac 3-sortintegertable % python3 test.py 
valeur 1 = 3
valeur 2 = 2
```
Pour la trie du tableau du coup a chaque valeur de la liste si elle est plus petite que celle d'après on récupère les deux et fait un swap.
Le problème c'est que ça demande de parcourir la liste n^n fois, du coup comme on l'a vue avec printcomb ce n'est pas fou niveau complexité de l'algorithme. 

![swap](./pics/swap.png)

On pourrait aussi parcourire la liste, enlever la plus petite valeur (avec la fonction `pop`) et l'ajouter a une autre liste. Recommencer n fois l'opération et on se retrouve avec la liste ordonnée.  

#### Seconde méthode:

Le plus simple ici était bien evidement d'utiliser la fonction de Python `sorted()`. À qui on donne en argument la liste à trier et qui nous retourne le travail fini ! <br>
 Mais c'est beaucoup moins amusant 😋

## code:
[suggestion de code](sortintegertable.py)
