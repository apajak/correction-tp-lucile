# TP-2 SortIntegerTable
# Alexandre Pajak

# Instructions:

# Write a function that reorders a slice of int in ascending order.


# Expected function:
# def sortIntegerTable(s list)-> list:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   integerTable = [5,4,3,2,1,0]
#   print(sortIntegerTable(integerTable))


# And its output :
# % python3 sortintegertable.py
# [0, 1, 2, 3, 4, 5]

### code ###

# Swap Method
def sortIntegerTable(integerTable: list) -> list:
    a = 0
    b = 0
    for _ in range(0, len(integerTable)):
        for index in range(0, len(integerTable) - 1):
            if integerTable[index] > integerTable[index + 1]:
                a = integerTable[index]
                b = integerTable[index + 1]
                integerTable[index] = b
                integerTable[index + 1] = a
    return integerTable


# The best Method ^^
# Simple is better than complex. (PEP20)
def sortIntegerTableV2(integerTable: list) -> list:
    return sorted(integerTable)


def main():
    integerTable = [5, 4, 3, 2, 1, 0]
    print(sortIntegerTable(integerTable))
    print(sortIntegerTableV2(integerTable))


main()
