# TP-2 sqrt
# Alexandre Pajak

# Instructions:

# Write a function that returns the square root of the int passed as parameter,
# if that square root is a whole number. Otherwise it returns 0.


# Expected function:
# def sqrt(number: int) -> int:

# Usage:
# Here is a possible program to test your function :
# def main() :
#   print(sqrt(4))
#   print(sqrt(3))


# And its output :
# % python3 fibonacci.py
# 2
# 0
# %

### code ###


def sqrt(number: int) -> int:
    if number > 0:
        for num in range(1, number):
            if num * num == number:
                return num


def sqrt_bis(number: int) -> int:
    if number >= 0:
        return int(pow(number, 0.5))


def main():
    print(sqrt(4))
    print(sqrt_bis(4))


main()
