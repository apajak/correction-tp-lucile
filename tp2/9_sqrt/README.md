# TP2

## Sqrt

### Objectif:
Faire un petit algorithme calculant la racine carré d'un nombre passer en argument. <br>

### Methode:

> La racine carré d'un nombre c'est aussi ce nombre a la puissance 0.5 ! 😉

Du coup et bien tous simplement j'ai utiliser ma fonction `pow(nombre, 0.5)`..

## code:
[suggestion de code](sqrt.py)
