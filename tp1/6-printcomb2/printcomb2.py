# TP-1 printcomb2
# Alexandre Pajak

# Instructions:
# Write a function that prints in ascending order and on a single line: all possible combinations of
# two different two-digit numbers.
# These combinations are separated by a comma and a space.

# Expected function:
# def PrintComb2():

# Usage:
# Here is a possible program to test your function :
# def main() :
#   Printcomb2()


# And its output :
# % python3 printcomb.py
# 00 01, 00 02, 00 03, ..., 00 98, 00 99, 01 02, 01 03, ..., 97 98, 97 99, 98 99$
# %


#### code: ####


def Printcomb2():  # fonction without argument beacause she do only one thing
    result = ""
    combList = []  # temp array

    for i in range(0, 100):  # first loop generate number between 0 an 99
        if i < 10:  # if less than 10 we add 0 befor the string convertion (7 become 07)
            comb1 = "0" + str(i)
        else:
            comb1 = str(i)  # else only convert

        for j in range(
            0, 100
        ):  # second loop run 99 times when first loop 1 time. the we have 00 01 ... 00 99
            # and at the second turne of first loop we have 01 00 ... 01 99
            if j < 10:
                comb2 = "0" + str(j)
            else:
                comb2 = str(j)
            if (
                comb1 != comb2 and comb1 < comb2
            ):  # if two combination is different and firs less than second we add it to combList
                combList.append(comb1 + " " + comb2)

    result = ", ".join(
        combList
    )  # for all in combList less last we concatenat it to result with comma and space
    print(result)


def main():
    Printcomb2()


main()
