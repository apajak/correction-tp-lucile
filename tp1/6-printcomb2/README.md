# TP1

## PrintComb

### Objectif:
Dans cet exercice nous avons besoin de générer tous les duos de combinaisons à deux chiffres dans lequel les duos sont différents et que le premier est inférieur au second.


### Methode:
Comme dans le dernier exercice nous avons besoin de gérer beaucoup de nombres... 
Donc on va faire des BOUCLES !
L'idée ici est d'imbriquer deux boucles l'une dans l'autre ainsi lorsque la boucle principale fait 1 tour la seconde boucle en fait autant qu'elle a moyen d'en faire. Il faut visualiser cela a la manière d'un compteur kilométrique de voiture ou encore une horloge à rouages. Tant que la seconde boucle n'a pas fini d'exécuter son code et bien la première patiente. Une fois terminer le premier recommence et la seconde également du coup ! 

Dans ces boucles nous allons donc générer un nombre entre 0 et 99 à fin de générer une string entre 00 et 99.
À chaque tour de la seconde boucle donc à chaque changement 00 01 -> 00 02 il ne nous reste plus qu'à vérifier si nos conditions sont respectées. Et si le comb1 est différent de la comb2 et que comb1 est inférieur à comb2 alors on ajoute au tableau temporaire "combList[]". 

Comme dans l'exercice précédent il ne reste qu'à afficher proprement.

donc:
> - Les boucles à la manière des conditions peuvent s'imbriquer.<br/>
> - Il faut bien faire attention à l'endroit ou l'on récupère notre résultat au milieu de toutes ces boucles car cela diffère énormément.<br/>
> - Les boucles sont ton meilleur allié pour traiter un grand nombre d'opérations.<br/>
> - Les tableaux où les listes permettent de stocker et manipuler de nombreuses informations. <br/>

#### Celui-là a dû te faire tourner la tete avec tous ces chiffres dans le terminal ! 🤪
## code:
[suggestion de code](printcomb2.py)
