# TP1

## PrintDigits

### Objectif:
Pour cet exercice le but est d'imprimé dans la console les chiffres de 0 à 9 sans espaces, retour à la ligne ou séparation.

### Methode:
Les nombres sont un type à part entière dans la programmation. Les integers (notés: int).
On peut manipuler les int de bien des manières mais la concaténation d'int ne fonctionne pas car en python 1+1 retournera 2.
Nous devons donc convertir notre int en str afin de pouvoir effectuer la concaténation.
Une fois converti nous pouvons les concaténer.
À fin d'automatiser la concaténation nous faisons une boucle for ranger. Elle est très utile pour répéter du code un nombre de fois défini. on donne à range en paramètre une range(Min, Max) et lorsque i sera dans cet intervalle alors la boucle sera exécuté.
exemple:
```py
for i in range(0,2): 
    print("coucou") 
```

> Les integers sont noté int et correspondent aux nombres entier.<br/>
> Les boucles for range sont efficace pour executer une opération un certain nombre de fois.<br/>
> Les int ne sont pas directement concaténable. Il faut donc changer le type de notre variable.<br/>

## code:
[suggestion de code](printdigits.py)
