# TP-1 printDigits
# Alexandre Pajak

# Instructions:
# Write a program that prints the decimal digits in ascending order (from 0 to 9) on a single line.

# Usage:
# % python3 printdigits.py
# 0123456789
# %


#### code: ####

# with for range function:


def printDigits():
    digits = ""  # intialise digits variable as string
    for i in range(0, 10):  # loop 10 times
        digits += str(i)  # digits append string convertion to int "i"
    print(digits)


printDigits()
