# TP1

## PrintComb (Récursive)

### Objectif:
Dans cet exercice nous avons besoin de générer toutes les combinaisons possibles pour "n" nombre tel que le premier est plus petit que le suivant. par rapport a la première version ici l'idée est de le faire en quelques dixièmes de seconde pour n=9 au lieu de 11 jours.


### Methode:
Ton Mac te remercier d'utiliser la récursivité ! 
L'idée change un peu par rapport au précédent printcombn (par force brute).
Ici l'idée est de trouver seulement les bonnes solutions au lieu de toutes les créer et de les vérifier par la suite. en utilisant des listes de chiffres afin de pouvoir manipuler chaque élément indépendamment. <br>
Donc : 
- On génère la première et dernière combinaison sous forme de liste de chiffres
- On crée une méthode pour trouver la suivante à partir de la première
- On répète la méthode jusqu'à atteindre la dernière solution
- Il ne reste plus qu'à afficher 🥹

#### Avant tout !
Déjà on commence par vérifier que l'argument est bien un int et est bien inférieur à 10 et supérieur à 0.
Puis du coup petit message d'erreur si ça n'est pas le cas.
Une fois cela fait on peut commencer à réfléchir. 

#### Pour générer la bonne solution n+1. 
Dans un code à 3 chiffres allant de 0 à 9 la première solution est forcément 012 c'est très simple à faire peu importe le n a construit une liste avec à l'intérieur ne nombres égaux à leurs indices.<br>
de la même manière la dernière combinaison, si on lit la liste en partant de la dernière indice chaque nombre sera égale à 9 moins son indice en partant de la fin. pour n=3 -> 789

Ayant tout cela maintenant il nous faut une fonction qui prend la combinaison d'avant. y ajoute +1 au bon element de la liste de chiffres. pour faire ça j'ai imaginé que le plus simple serait de partir du principe que : <br> 
- si l'indice [-1] de la combinaison n-1 est inférieur à 9 alors ce sera là qu'on ajoutera le +1
- Une fois arriver à 9 il vaut du coup décaler d'un indice vers la gauche. Faire le +1 puis pour tous les index suivants à droite recommencer avec la valeur de l'index que je viens de changer +1 à chaque fois que je décale vers la droite. un exemple sera bien mieux je suis d’accord !
```py
def main():
    # n must be :  0 < n < 10 
    n = 3
    PrintCombN(firstComb(n) ,n)
```
le résultat:
```py
...
[0, 2, 9] 
[0, 3, 4]
...
```
Je ne peux pas faire mon +1 à 9 alors je regarde si je peux le faire à coter. Oui donc je fais 2+1 puis mon 9 devient du coup 3+1.
```py
def main():
    # n must be :  0 < n < 10 
    n = 3
    PrintCombN(firstComb(n) ,n)
```
le résultat:
```py
...
[0, 8, 9]
[1, 2, 3]
...
```
Ici 8 étant la limite de cet index alors ce sera sur le 0 que je ferais mon +1 et du coup 8 devient 2 et 9=3
Il faut visualiser cela vraiment comme une horloge ou un compteur kilométrique de voiture. si la première roue fait un tour complet alors la suivante avance de 1 et la première revient à 0 (sauf qu'ici on reprend là où en est la précédente histoire de toujours avoir le chiffre supérieur  à celui d'avant)<br>

Bon maintenant qu'on arrive à ajouter 1 là où il le faut et bien il ne reste plus qu'à automatiser tout ça. La récursivité est parfaite pour ce genre de problème car elle permet de répéter ma fonction en donnant en entrée la sortie de la précédente. Comme cela elle calcule à chaque fois la combinaison suivante avec pour condition d'arrêt le fait de tomber sur la combinaison finale qu'on a facilement calculée plus tôt.<br>

On ajoute à chaque exécution de la fonction la combinaison (une liste) a la liste de combinaison et donc on a une liste de listes qu'il nous reste plus qu'à afficher.<br>

donc:
> - Il est important de prendre en compte la complexité (au sens informatique) d'un programme. La complexité c'est le nombre de calcul élémentaire qu'il va falloir à une machine pour résoudre un problème. Lorsqu'on a besoin de performance on doit bien faire attention à réduire au maximum les calcule et les grandes boucles. Pour n = 9 rien que générer les combinaisons possibles ça fait 1000000000 des calculs. On ajoute à cela que pour chaque combinaison on doit comparer le premier nombre avec le second et ainsi de suite ce qui fait 1000000000 x 10 fin bref c'est l'enfer... 
> - La récursivité est souvent bien utile pour simplifier des tâches complexes.

#### Promis on arrête vraiment avec les listes de nombres ! 
## code:
[suggestion de code](printcombn.py)
