# TP-1 printAlphabet
# Alexandre Pajak

# Instructions:
# Write a program that prints the Latin alphabet in lowercase on a single line.

# Usage:
# % python3 printalphabet.py
# abcdefghijklmnopqrstuvwxyz
# %


#### code: ####

# with print() function:
from pyparsing import indentedBlock


def printalphabet():
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    mot = []
    index = 0
    while index < 10:
        mot.append(alphabet[index])
        index -= 1
    print(mot)


# run printalphabet()
printalphabet()
