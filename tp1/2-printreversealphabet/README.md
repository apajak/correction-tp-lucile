# TP1

## PrintReversAlphabet

### Objectif:
Ici le but est d'afficher les lettres de l'alphabet dans le sens inverse, alignées et sans séparation.

### Methode:
Pour afficher du texte dans le terminal on utilise la fonction print() qui prend en argument des Strings et les retourne dans le terminal.

Une string en Python peut être parcourue de la même manière qu'un tableau ou une liste. Chaque lettre qui compose une string peut donc etre récupérée individuellement grace a son index (sa position dans la chaine de caractères en partant de 0).

Dans un second temps en Python les strings peuvent se "concaténer". c'est a dire que l'on peut additionner des strings entre elles.

exemple:
```py
str1 = "Salut"
str2 = "Lucile"
str3 = srt1 + str2
print(str3) #retourne "SalutLucile" 
```
Il nous suffit donc de parcourir la liste de lettre dans le sens inverse et de les ajouter à notre variable reversAlphabet.
par exemple:
```py
mot = "Lucile"
lettre = mot[0] 
print(lettre)#retourne "L"
lettre = mot [2] 
print(lettre)#retourne "c"
```

> Les Strings sont notées "str" en Python.<br/>
> La fonction len() permet de recupérer la taille d'un objet.<br/>
> La boucle While permet de réiterrer une action un certain nombre de fois.<br/>
> Chaque lettre qui compose une string est semblable à un élèment d'un tableau et possède donc un index.<br/>

## code:
[suggestion de code](printreversealphabet.py)
