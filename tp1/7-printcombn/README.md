# TP1

## PrintComb

### Objectif:
Dans cet exercice nous avons besoin de générer toutes les combinaisons possibles pour "n" nombre tel que le premier est plus petit que le suivant.


### Methode:
Aïe ! Ton Mac fume... 
Ok celui-là, pas simple non plus ! Mais il faut s'habituer à gérer de grandes listes car cela oblige à faire confiance à son algorithme et faire de belles boucles ("for range" "while"...).
À moins d'avoir beaucoup de temps pour le faire a la main . 🤔
Pour info mon code (qui n'est pas le meilleur) met 11 jours pour calculer printcombn(9) 🤯
C'est beaucoup trop dans l'absolu, Mais ici l'important c'était de s'entrainer en algorithmie et de montrer une solution envisageable par tout le monde sans utiliser de librairies spécialisées.

Alors l'idée est la même que pour le printcomb.
- On génère les nombres
- On trie ses nombres
- On les affiches 

#### Avant tout !
Déjà on commence par vérifier que l'argument est bien un int et est bien inférieur à 10 et supérieur à 0.
Puis du coup petit message d'erreur si ça n'est pas le cas.
Une fois cela fait on peut commencer à réfléchir à komen kon fait. 🧐

#### Pour générer la bonne quantité de nombre. 
Dans un code à 3 chiffres allant de 0 à 9 il y a 10 puissances 3 solutions
Donc on génère 10 puissances ne nombrent pas.Ça c'est ma boucle principale avec la variable power.


- Boucle principale 0 -> 10^n
    - J'y ai ajouté deux petites lignes pour suivre la progression car c'était long et je n'avais pas 11 jours devant moi...
    - Je créais trois variables: numberlen un int qui me sert à savoir de combien de chiffre est composé mon nombre. Le second index un int encore qui me servira à adapter le calcule d'unité aux unités dizaine centaine etc. la dernière un tableau unitlist qui lui servira à récupérer mes unités une fois calculées.
    - Boucle secondaire qui elle commence à 0 et s'arrête a numberlen. l'idée et de bouclé dans le nombre pour récupérer les unités qui le compose. Lorsque notre boucle principale atteint le nombre 100, numberlen sera égale à 3 et donc je ferais trois fois le calcule d'unité pour récupérer unité, dizaine, centaine. À chaque tour j'enregistre mon résultat dans le tableau unit List[]. Elles sont enregistrées à l'envers car je commence par récupérer les unités. 123 devient [3,2,1]
    - Pour les chiffres j'ajoute directement le résultat à un nouveau tableau comblist car il sera forcément bon.
    - Pour les nombres je dois par contre vérifier la condition "unité < dizaine < centaines < milliers". <br/> pour faire cela je boucle dans mon tableau d'unité (unit List), qui est à l'envers on n'oublie pas ! Et si l'unité à l'index 0 est inférieure ou égale à l'index suivant dans la liste alors la combinaison n'est pas bonne. Dans l'exemple de 123 donc [3,2, 1] on voit bien qu'il faut que le premier élement de la liste soit strictement supérieur au suivant. <br/> On fait cette comparaison n >= a n+1 et dès qu'il ne remplit pas la condition on stope la boucle grace à "break" et on n'enregistre rien on passe au nombre suivant. Dans l'autre cas si on arrive à l'avant-dernier index et que celui-ci est plus grand que le dernier element de la liste alors cela veut dire qu'ils sont tous bons. On les ajoute donc dans la liste de bonnes combinaisons comb List (dans le bon ordre cette fois-ci^^)
    - Maintenant que je suis sorti de cette troisième boucle avec mes bonnes combinaisons il me reste toujours un problème... La consigne veut que si n= 3 l'affichage pour le nombre 1 doit être 001. alors c'est reparti pour une boucle... . <br/> Je boucle donc tant que (antwhile) la taille de ma comb est inférieure à 10^n MOINS 1 car 10^2 ça fait 100 mais le 0 étant une combinaison on ne va que de 00 à 99 donc dizaine unité. À chaque tour j'ajoute donc un zéro avant ma comb jusqu'à qu'il en ait assez.
- Ouf on en a fini on a notre belle liste comblist[] il ne reste plus qu'à l'afficher comme on l'a déjà fait ! 

donc:
> - Il est important de créer des expressions mathématiques avec nos variables à fin que peu importe le nombre de fois que le calcule sera lancé et la valeur de ses variables le résultat soit toujours bon. Ça vaut le coup de passer du temps à trouver la bonne formule mathématique (y compris a l'aide d'internet bien sur!) pluto de faire plusieurs petits calcule qui une fonction seulement dans certains cas. Ici nous avons fait une expression qui est capable de trouver aussi bien les unités que les centaines.
> - Une nouvelle doit faire attention à l'indentation dans les boucles pour obtenir le nombre d'exécution voulu.

#### Promis on arrête quelque temps avec les listes de nombres ! 
## code:
[suggestion de code](printcombn.py)